import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import $ from 'jquery';





class Square extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          index: props.index
        };       
    }

    render() {
      var styleClass;
      switch (this.props.value) {
        case "1":
          styleClass="red";
          break;
        case "2":
          styleClass="blue";
          break;
        case "3":
          styleClass="green";
          break;
        case "4":
          styleClass="orange";
          break;
        case "5":
          styleClass="purple";
          break;
        case "6":
          styleClass="black";
          break;
        default:
          break;
    }


      if (!(this.props.marked1==this.state.index || this.props.marked2==this.state.index )) {
          return (
            <button className={styleClass} onClick={() => this.props.onClick()}>
              
            </button>
          );
        }
      else {
        //alert(this.props.marked1+" = "+this.state.index+" || "+this.props.marked2+" = "+this.state.index);
        var markedStyle= {
          borderStyle: "dashed",
          borderWidth: "5px",
          borderColor: "#000000"
        }
        return (
          <button className={styleClass} style={markedStyle} onClick={() => this.props.onClick()}>
            
          </button>
        );

        }
    }
}

  
  class Board extends React.Component {

    constructor(props) {


        super(props);
        this.state = {
          squares: (""+props.board).split(","),
          token: props.token,
          name: props.name,
          score: 0,
          marked1: -1,
          marked2: -1
        };

        //convert to string then split by comma
        //var chrArr=;
        //this.state.squares=chrArr;

    }


    
    //get coordinates for index
    getCoordinates(i) {
      var y=(i/9);
      y=Math.floor(y);

      var x= i- Math.floor(i/9)*9;

      var coordinates={
        x: x, 
        y: y
      };

      return coordinates;
    }

    checkNeighbour(coor1,coor2) {
      if (Math.abs(coor1.x-coor2.x)==1  && coor1.y==coor2.y) return true;
      if (Math.abs(coor1.y-coor2.y)==1  && coor1.x==coor2.x) return true;
      return false;
    }
  
    //kljucni igrac
    handleClick(i) {
      
        
        if (this.state.marked1 == i) return;
       

        //if one is allready marked
        if (this.state.marked1>-1) {             
            var coordinatesClicked=this.getCoordinates(i);
            //alert("Clicked: x: "+coordinatesClicked.x+" y: "+coordinatesClicked.y);
  
            var coordinatesMarked=this.getCoordinates(this.state.marked1);
            //alert("Marked: x: "+coordinatesMarked.x+" y: "+coordinatesMarked.y);

            if (this.checkNeighbour(coordinatesClicked,coordinatesMarked)) {
              

              this.setState({
                marked1: this.state.marked1,
                marked2: i,
              });

              //alert("AJAX SWAP CALL");
              var token=this.state.token;
              var url= 'http://localhost:8080/axilis-game.azurewebsites.net/game/swap';  
              var element=this;

              $.ajax({
                async: "false",
                type: 'POST',
                            data: JSON.stringify({
                              "token":token,
                              "row1":coordinatesClicked.y,
                              "col1":coordinatesClicked.x,
                              "row2":coordinatesMarked.y,
                              "col2":coordinatesMarked.x,
                            }),
                            contentType: 'application/json',
                            url: url,					
                            success: function(data) {                                                 
                              var chrArr=(""+data.board).split(",");
                              var newScore=data.totalScore; 
                              var gameOver=data.gameOver;
                              if (gameOver) alert("gameOver");
                              setTimeout(()=>{
                                element.setState({
                                  marked1: -1,
                                  marked2: -1,
                                  squares: chrArr,
                                  score: newScore
                                });
                              }, 1);
                            },
                            error: function(err) {
                              setTimeout(()=>{
                                element.setState({
                                  marked1: -1,
                                  marked2: -1,
                                });
                              }, 1);                            
                            }
                });
            }
            else {
              this.setState({
                marked1: i,
                marked2: -1
              });
            }
        }

        else {
          this.setState({
            marked1: i,
            marked2: -1
          });

        }




    }

    renderSquare(i) {
        return <Square 
                       value={this.state.squares[i]} 
                       index={i}
                       onClick={()=> this.handleClick(i)}
                       marked1={this.state.marked1}
                       marked2={this.state.marked2}
        />;
    }
  
    render() {
      var boldStyle= {
        fontWeight:"bold"
      }
      let status="NAME: "+this.state.name+", SCORE: "+this.state.score;
      return (      
        <div>
          <div className="status" style={boldStyle}>{status}</div>
          <div className="board-row">
            
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
          <div className="board-row">
            {this.renderSquare(9)}
            {this.renderSquare(10)}
            {this.renderSquare(11)}
            {this.renderSquare(12)}
            {this.renderSquare(13)}
            {this.renderSquare(14)}
            {this.renderSquare(15)}
            {this.renderSquare(16)}
            {this.renderSquare(17)}
          </div>
          <div className="board-row">
            {this.renderSquare(18)}
            {this.renderSquare(19)}
            {this.renderSquare(20)}
            {this.renderSquare(21)}
            {this.renderSquare(22)}
            {this.renderSquare(23)}
            {this.renderSquare(24)}
            {this.renderSquare(25)}
            {this.renderSquare(26)}
          </div>

          <div className="board-row">
            {this.renderSquare(27)}
            {this.renderSquare(28)}
            {this.renderSquare(29)}
            {this.renderSquare(30)}
            {this.renderSquare(31)}
            {this.renderSquare(32)}
            {this.renderSquare(33)}
            {this.renderSquare(34)}
            {this.renderSquare(35)}
          </div>
          <div className="board-row">
            {this.renderSquare(36)}
            {this.renderSquare(37)}
            {this.renderSquare(38)}
            {this.renderSquare(39)}
            {this.renderSquare(40)}
            {this.renderSquare(41)}
            {this.renderSquare(42)}
            {this.renderSquare(43)}
            {this.renderSquare(44)}
          </div>
          <div className="board-row">
            {this.renderSquare(45)}
            {this.renderSquare(46)}
            {this.renderSquare(47)}
            {this.renderSquare(48)}
            {this.renderSquare(49)}
            {this.renderSquare(50)}
            {this.renderSquare(51)}
            {this.renderSquare(52)}
            {this.renderSquare(53)}
          </div>

          <div className="board-row">
            {this.renderSquare(54)}
            {this.renderSquare(55)}
            {this.renderSquare(56)}
            {this.renderSquare(57)}
            {this.renderSquare(58)}
            {this.renderSquare(59)}
            {this.renderSquare(60)}
            {this.renderSquare(61)}
            {this.renderSquare(62)}
          </div>
          <div className="board-row">
            {this.renderSquare(63)}
            {this.renderSquare(64)}
            {this.renderSquare(65)}
            {this.renderSquare(66)}
            {this.renderSquare(67)}
            {this.renderSquare(68)}
            {this.renderSquare(69)}
            {this.renderSquare(70)}
            {this.renderSquare(71)}
          </div>
          <div className="board-row">
            {this.renderSquare(72)}
            {this.renderSquare(73)}
            {this.renderSquare(74)}
            {this.renderSquare(75)}
            {this.renderSquare(76)}
            {this.renderSquare(77)}
            {this.renderSquare(78)}
            {this.renderSquare(79)}
            {this.renderSquare(80)}
          </div>

          
        </div>
      );
    }
  }
  
  //game component
  class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

          }

          this.startGame = this.startGame.bind(this);
        };
    


  startGame() {
    //ajax call to get initial state
    var url= 'http://localhost:8080/axilis-game.azurewebsites.net/game/new';
    var token=this.props.userToken;
    var element=this;

    $.ajax({
      type: 'POST',
                  data: JSON.stringify({"token":token}),
                  contentType: 'application/json',
                  url: url,					
                  success: function(data) {

                      //element.setState({gameToken: data.gameToken});
                      //render board
                      ReactDOM.render(
                        <div className="game">
                        <div className="game-board">
                          <Board token={data.gameToken} board={data.board} name={element.props.name}/>
                        </div>
                        <div className="game-info">
                          <div>{}</div>
                          <ol>{}</ol>
                        </div>
                      </div>,
                      document.getElementById('root')
                      ); 

                  },
                  error: function(err) {
                    alert(JSON.stringify(err));                   
                  }
      });


       
  }


    render() {
      return (
        <div className="welcome">
            <label className="welcomeMessage">             
              Welcome {this.props.name.split(" ")[0]}
              <br></br>
              <button className="startGame" onClick={() => this.startGame()}>Start game</button>
            </label>
        </div>
      );
    }


  }


  //name and email form component
  class NameForm extends React.Component {
    constructor(props) {
      super(props);


      
      this.state = {
          name: '',
          email: '',
          userToken: '',
        };
      
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleEmailChange = this.handleEmailChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);


    }

    
  
    handleNameChange(event) {
      this.setState({name: event.target.value});
    }

    handleEmailChange(event) {
        this.setState({email: event.target.value});
    }
  
    handleSubmit(event) {
      event.preventDefault();


      //communication to REST API to get token
      //render game when token is ready      
      var name=this.state.name;
      var email=this.state.email;  
      var url= 'http://localhost:8080/axilis-game.azurewebsites.net/register';
      var element=this;


      $.ajax({
        type: 'POST',
                    data: JSON.stringify({"name":name,"email":email}),
                    contentType: 'application/json',
                    url: url,					
                    success: function(data) {
                        var token=data.token;
                        element.setState({userToken: token});
                        //render game when token is ready
                        //render welcome!
                        ReactDOM.render(
                          //<Game />,
                          <Game userToken={token} name={name} email={email}/>,
                          document.getElementById('root')
                        );
                    },
                    error: function(err) {
                      alert(JSON.stringify(err));                   
                    }
                });
           
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <label>
            Name:
            <input type="text" value={this.state.name} onChange={this.handleNameChange} />
          </label>
          <label>
            Email:
            <input type="text" value={this.state.value} onChange={this.handleEmailChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      );
    }
  }
  
  // ========================================



  ReactDOM.render(
    
    //<Game />,
    <NameForm/>,
    document.getElementById('root')
  );